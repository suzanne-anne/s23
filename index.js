// console.log('Hello');

let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoen: ['May', 'Max'],
		kanto: ['Brock', 'Misty']
	},

	talk: function(){
	console.log('Pikachu! I choose you!');
	}

}

console.log(trainer);

console.log('Result of dot notation:')
console.log(trainer.name);

console.log('Result of square bracket notation:')
console.log(trainer.pokemon);

console.log('Result of talked method:')
trainer.talk();

function Pokemon(name, level) {

    this.name = name;
    this.level = level; 
    this.health = 2 * level; // 4 health
    this.attack = level;

    //Methods
    this.tackle = function(target) {
        console.log(this.name + ' tackled ' + target.name);
        target.health -= this.attack; 
        console.log(target.name + "'s health is now reduce to " + target.health);

        if(target.health <= 0){
            target.faint();
        }
    };

    this.faint = function(){
        console.log(this.name + ' fainted')

    }
}


let pikachu = new Pokemon('Pikachu', 12);
let geodude = new Pokemon('Geodude', 8);
let mewto = new Pokemon('Mewto', 100);

console.log(pikachu)
console.log(geodude)
console.log(mewto)


geodude.tackle(pikachu);
console.log(pikachu)

mewto.tackle(geodude);
console.log(geodude)